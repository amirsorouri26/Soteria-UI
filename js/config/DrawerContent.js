import React, {Component} from 'react';
import  {
    Text,
    View,
    FlatList,
    Image,
    Dimensions,
    StyleSheet,
    Button,
    ScrollView,
    Animated,
    TouchableWithoutFeedback,
    LogoHeader
    ,

} from 'react-native'
import DrawerItem from './DrawerItem'
import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
import {TabNavigator, StackNavigator, TouchableOpacity, DrawerNavigator, TabBarBottom} from 'react-navigation';
import mycontract from '../screen/myContracts'
import {Avatar} from 'react-native-elements'
import {soteria} from './routers'
var navitems = [
    {
        name: 'emssspty class schedule',
        nav: 'Home',
    },
    {
        name: 'settings',
        nav: 'Appointment',
        //path:'/schedule'
    }, {
        name: 'settings',
        nav: 'Settings',
        //path:'/schedule'
    },
]

//this is the custom component i want to put in the drawer, can be any component of course


class DrawerContent extends Component {
    constructor(props) {
        super(props)
    }

    render() {


        var {height, width} = Dimensions.get('window');
        console.warn(height)
        return (
            <View style={{borderWidth:0 , height:height}}>
                <AnimatedLinearGradient customColors={[ 'rgb(14, 158, 181)',
    'rgb(91, 64, 148)']} speed={4000}/>


                <View style={{  height:172}}>
                    <Image style={{  backgroundColor: '#ccc',
          flex: 1,
          resizeMode :'center',
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center'}} source={require('../img/back.jpg')}>
                        <View style={{

                 flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
         }}>

                            {/*<AnimatedLinearGradient customColors={[ 'rgb(74, 64, 99)',*/}
                            {/*'rgb(91, 64, 179)']} speed={4000}/>*/}


                            <View style={{
                             }}>
                                <Image source={require('../img/avata.png')} style={{borderRadius:50
                             , width:100, height:100,
                             borderWidth: 5,
                             borderColor:'#ffffff'}}/>

                                <Text style={{fontWeight: 'bold' ,color:'white'}}>Ethers:12</Text>
                                <Text style={{fontWeight: 'bold' ,color:'white'}}>Soters:43</Text>
                                {/*<Avatar*/}
                                {/*large*/}
                                {/*rounded*/}

                                {/*source={require('../img/avata.png')}*/}
                                {/*onPress={() => console.log("Works!")}*/}
                                {/*activeOpacity={0.7}*/}
                                {/*/>*/}
                            </View>

                        </View>
                    </Image>
                </View>

                <View>


                </View>

                <ScrollView>
                    <DrawerItem title="Wallet" navigation={this.props.navigation}/>

                    <DrawerItem title="AboutUs" navigation={this.props.navigation}/>
                    <DrawerItem title="Exit" navigation={this.props.navigation}/>

                </ScrollView>
                {/*<View>*/}
                {/*{*/}
                {/*navitems.map((l, i) => {*/}
                {/*return (*/}

                {/*<Button title={l.name} key={i} style={{marginBottom:0.5}} onPress={()=>{*/}
                {/*this.props.navigation.navigate(l.nav)*/}
                {/*}}>*/}

                {/*</Button>)*/}
                {/*})*/}
                {/*}*/}
                {/*</View>*/}

            </View>
        )
    }
}

import {DrawerRoutesWallet} from './routers'
export default DrawerNavigator(DrawerRoutesWallet,
    {
        initialRouteName: 'Wallet',
        contentComponent: ({navigation}) => <DrawerContent navigation={navigation} routes={DrawerRoutes}/>, //you dont need the routes props, but just in case you wanted to use those instead for the navigation item creation you could

    }
)
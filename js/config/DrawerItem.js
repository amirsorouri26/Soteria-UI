/**
 * Created by mohsen on 8/2/2017.
 */
import React, {Component} from 'react';
import  {Text, View, Button, TouchableOpacity} from 'react-native'
import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
import {Divider} from 'react-native-elements';

export default class DrawerItem extends Component {


    constructor(props) {
        super(props)
        this.btnclick = this.btnclick.bind(this)
        this.btnLogIn = this.btnLogIn.bind(this)
    }

    state = {
        title: this.props.title
    }

    btnclick() {
        console.warn('iam hear')
        this.props.navigation.navigate(this.state.title)
        // this.setState({test: !this.state.test})
    }

    btnLogIn() {

        this.props.navigation.navigate('MainPage')
    }

    render() {

        return (

            <View>
                <View
                    style={{  flex:1,flexDirection: 'row' , justifyContent:'center',   alignItems: 'center' ,height:60   }}>
                    <Text style={{marginLeft:5, fontSize:17 , color:'white'}}>{this.props.title}</Text>
                </View><Divider style={{ backgroundColor: 'white' }}/>
            </View>

        )

    }
}
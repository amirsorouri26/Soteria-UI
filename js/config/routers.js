/**
 * Created by mohsen on 8/2/2017.
 */
import React from 'react'
import {Text} from 'react-native'
import {TabNavigator, StackNavigator, DrawerNavigator, TabBarBottom} from 'react-navigation';
import LogIn from '../screen/LogIn'
import Activities from '../screen/Activities'
import SwipeoutExample from'../test/react-native-swipeout/test'
import ViewPagerPage from'../test/viewpager/test'
import MainScreen from'../test/react-native-viewpager/MainScreen'
import ExampleView from'../test/react-native-animatable/app'
import Activity from '../screen/member/Activity'
import Chatroom from '../screen/chatroom/chatroom'
import  activityItem from '../screen/activityItem'
import Detail from '../screen/member/Detail'
import lottie from '../test/lottie/LottieAnimatedExample'
import  interactable from '../test/react-native-interactable/app'
import Splash from '../screen/splash'
import {connect} from 'react-redux'
import MyContracts from '../screen/myContracts'
import CreateContract from '../screen/createContract'
import Contribute from '../screen/contribute'
// import customizeNavigation from './DrawerContent'
import contractDetail from '../screen/contract/ContractDetail'

import  {Component} from 'react';
import  {
    TouchableOpacity,
    View,
    FlatList,
    Image,
    Dimensions,
    StyleSheet,
    Button,
    ScrollView,
    Animated,
    TouchableWithoutFeedback,
    LogoHeader
    ,

} from 'react-native'
import DrawerItem from './DrawerItem'
import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'

import mycontract from '../screen/myContracts'
import {Avatar} from 'react-native-elements'


export const ActivityTabs = TabNavigator({

    Detail: {
        screen: Detail
    },
    Activity: {
        screen: Activity


    }
}, {

    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom'

})

const contractDetailNavigator = StackNavigator({
    Main: {screen: MyContracts},
    detail: {screen: contractDetail},
});
export const ActivityRouter = StackNavigator({
    Activities: {
        screen: Activities,
    },
    Activity: {

        screen: ActivityTabs
    },

}, {

    headerMode: 'none'

})
export const soteria = TabNavigator({
        Wallet: {
            screen: contractDetailNavigator
        }
        , Contribute: {
            screen: Contribute
        }, makeContract: {
            screen: CreateContract
        }
    },
    {
        tabBarPosition: 'bottom',
        // animationEnabled: true,
        tabBarOptions: {
            inactiveTintColor: '#a0a29e',
            labelStyle: {
                paddingTop: -2,
                fontSize: 12,
            },
            tabStyle: {
                paddingTop: 15,
                height: 56,
            },
            style: {
                borderTopWidth: 0,
                // elevation: 4,
                // marginBottom: 5,
                backgroundColor: '#e0e0e0',
            },
            activeTintColor: '#1f4a5b',
            showIcon: true,
        }
    })
// export const DrawerMenu = DrawerNavigator({
//     Main: {
//         screen: soteria,
//     }, chatroom: {
//         screen: Chatroom,
//     },
//     Activities: {
//         screen: ActivityRouter,
//     }, animatable: {
//         screen: ExampleView,
//     },
//     SwipeoutExample: {
//         screen: SwipeoutExample,
//     },
//     ViewPagerPage: {
//         screen: ViewPagerPage,
//     },
//     viewpager: {
//         screen: MainScreen,
//     }, interactable: {
//         screen: interactable,
//     }, Lottie: {
//
//         screen: lottie
//     },
// });
export const DrawerRoutesWallet = ({
    Wallet: {
        screen: soteria
    },
    AboutUs: {
        screen: soteria
    },
    Exit: {
        screen: soteria
    },
})


class DrawerContent extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        var {height, width} = Dimensions.get('window');
        console.warn(height)
        return (
            <View style={{borderWidth:0 , height:height}}>
                <AnimatedLinearGradient customColors={[ 'rgb(14, 158, 181)',
    'rgb(91, 64, 148)','rgb(0, 247, 255)']} speed={3000}/>


                <View style={{  height:172}}>
                    <Image style={{  backgroundColor: '#ccc',
          flex: 1,
          resizeMode :'center',
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center'}} source={require('../img/back.jpg')}>
                        <View style={{

                 flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
         }}>
                            <View style={{
                             }}>
                                <Image source={require('../img/avata.png')} style={{borderRadius:50
                             , width:100, height:100,
                             borderWidth: 5,
                             borderColor:'#ffffff'}}/>

                                <Text style={{fontWeight: 'bold' ,color:'white'}}>Ethers:12</Text>
                                <Text style={{fontWeight: 'bold' ,color:'white'}}>Soters:43</Text>

                            </View>

                        </View>
                    </Image>
                </View>

                <View>


                </View>

                <ScrollView>
                    <DrawerItem title="Wallet" navigation={this.props.navigation}/>
                    <DrawerItem title="AboutUs" navigation={this.props.navigation}/>
                    <DrawerItem title="Exit" navigation={this.props.navigation}/>
                </ScrollView>
            </View>
        )
    }
}
const customizeNavigation = DrawerNavigator(DrawerRoutesWallet,
    {
        initialRouteName: 'Wallet',
        contentComponent: ({navigation}) => <DrawerContent navigation={navigation}/>, //you dont need the routes props, but just in case you wanted to use those instead for the navigation item creation you could

    }
)


export const MainRoute = StackNavigator({
    Splash: {

        screen: Splash
    },
    LogIn: {

        screen: customizeNavigation
    },
    MainPage: {
        screen: LogIn
    }
    /*   ,
     MainPage: {
     screen: DrawerMenu
     }*/

}, {

    headerMode: 'none'

})


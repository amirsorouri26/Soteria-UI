/**
 * Created by mohsen on 8/2/2017.
 */
import React, {Component} from 'react';
import  {Text, Image, Dimensions, View,StatusBar, Button} from 'react-native'
import TimerMixin from 'react-timer-mixin';

import AnimatedLinearGradient, {presetColors} from 'react-native-animated-linear-gradient'
export default class Splash extends Component {
    static navigationOptions = {
        title: 'Login'
        ,

    }
    mixins: [TimerMixin]

    componentWillMount() {
        var navigator = this.props.navigation;
        console.log(this.props)

    }

    constructor(props) {
        super(props)
        this.btnclick = this.btnclick.bind(this)
        this.btnLogIn = this.btnLogIn.bind(this)
    }

    state = {
        test: false
    }

    btnclick() {
        this.setState({test: !this.state.test})
    }

    btnLogIn() {

        this.props.navigation.navigate('MainPage')
    }

    render() {
        var {height, width} = Dimensions.get('window');
        let count = 0;
        var navigator = this.props.navigation;
        setInterval(() => {
            count++;
            if (count == 2) {
                console.log('hear  dd')
                navigator.navigate('LogIn');
            }
        }, 3000);


        return (
            <View style={{borderWidth:0 , height:height, flex: 1,
    justifyContent: 'center', alignItems:'center'}}>
                <StatusBar

                    translucent
                />
                <AnimatedLinearGradient style customColors={['rgb(124,3,108)','rgb(181,0,84)','rgb(115,70,33)']} speed={2000}/>
                <Image
                    source={require('../img/splash.png')}/>

            </View>



        )

    }
}
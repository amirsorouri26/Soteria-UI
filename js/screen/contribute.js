/**
 * Created by mohsen on 8/2/2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux'
import  {Text, View,Image, Button,StyleSheet} from 'react-native'
import TimerMixin from 'react-timer-mixin';
export  class Contribute extends Component {
    static navigationOptions = {
        tabBarLabel: 'Participate  ',
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../img/contrbute.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    mixins: [TimerMixin]

    componentWillMount() {
        var navigator = this.props.navigation;
        console.log(this.props)

    }

    constructor(props) {
        super(props)
        this.btnclick = this.btnclick.bind(this)
        this.btnLogIn = this.btnLogIn.bind(this)
    }

    state = {
        publickey: this.props.wallet.publicCode
        ,
        soter: this.props.wallet.soters
    }

    btnclick() {
        this.setState({test: !this.state.test})
    }

    btnLogIn() {

        this.props.navigation.navigate('MainPage')
    }

    render() {
        let count = 0;
        console.log('walletstate', this.state)

        return (
            // <View>
            //     <RkCard style={{margin : 10}}>
            //         <View rkCardHeader>
            //             <Text>{props.contract.address}</Text>
            //         </View>
            //         <View rkCardContent>
            //             <RkText rkType='primary'> Buyer  -> {props.contract.buyer}</RkText>
            //             <RkText rkType='primary'> Seller -> {props.contract.buyer}</RkText>
            //             <RkText rkType='success'> Balance is {props.contract.balance.toString()}</RkText>
            //             <RkText> Contract is <Text style={{color: '#'}}>{props.contract.contractState} </Text>
            //                 <RkText> Contract created on {props.contract.date.toLocaleDateString()} </RkText>
            //
            //             </RkText>
            //         </View>
            //
            //         <View rkCardFooter>
            //         </View>
            //     </RkCard>
            // </View>
        // )
    <Text> Contribute here</Text>)
    }
}
const mapStateToPropsMainWallet = (state) => {
    return {
        wallet: state.soteria.myWallet
    }
}
const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25,

    },
});
export default connect(mapStateToPropsMainWallet)(Contribute);
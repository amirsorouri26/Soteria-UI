/**
 * Created by mohsen on 8/2/2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux'
import  {Text, View, Image, Button,StyleSheet } from 'react-native'
import TimerMixin from 'react-timer-mixin';
import {RkButton, RkTextInput, RkTheme} from 'react-native-ui-kitten';
export  class CreateContract extends Component {
    static navigationOptions = {
        title: 'create '
        ,
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../img/create.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };




    mixins: [TimerMixin]

    componentWillMount() {
        var navigator = this.props.navigation;
        console.log(this.props)

    }
    constructor(props) {
        super(props);
        this.state = {
            fromAddr: '0xfdgdfgdfgfdbc',
            amount: 0.5,
            gasFee: 0.015
        }
    }


    state = {
        publickey: this.props.wallet.publicCode
        ,
        soter: this.props.wallet.soters
    }

    btnclick() {
        this.setState({test: !this.state.test})
    }

    btnLogIn() {

        this.props.navigation.navigate('MainPage')
    }

    render() {
        let count = 0;
        console.log('walletstate', this.state)

        return (
            <View style={styles.container}>
                <RkTextInput label='From' rkType='success' value={this.state.fromAddr} onChangeText={
                    (fromAddr) => this.setState({fromAddr})
                }/>
                <RkTextInput label='Amount' value={this.state.amount} onChangeText={
                (amount) => this.setState({amount})
                } rkType='success'/>
                <RkTextInput label='Gas fee' keyboardType='numeric' value={this.state.gasFee.toString()}
                             rkType='success'
                             onChangeText={(gasFee) => {
		            this.setState({gasFee : gasFee});
		        }}/>

                <RkButton onClick={(e) =>Alert(this.state)}>Send</RkButton>

            </View>
        )

    }
}
const mapStateToPropsMainWallet = (state) => {
    return {
        wallet: state.soteria.myWallet
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin:10,
        backgroundColor: '#fff',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',

    },
    button1: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    icon: {
        width: 25,
        height: 25,

    },
});


export default connect(mapStateToPropsMainWallet)(CreateContract);
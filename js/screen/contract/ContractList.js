import React from 'react'
import {View, Image, Text, FlatList, TouchableWithoutFeedback} from 'react-native'
import Contract from './Contract'
import {
    RkText,
    RkChoiceGroup,
    RkChoice,
    RkTheme,
    RkCard,
    RkSeparator
} from 'react-native-ui-kitten';
import photo from '../../img/back.jpg'

export default class ContractList extends React.Component {
    render() {
        console.log(this.props)
        return (
            <FlatList
                data={this.props.contracts}
                renderItem={({item}) =>{
              console.log(item)
                  return  <Contract  navigation={this.props.navigation} contract={item}/>}
                }
            />
        )
    }
}



import React from 'react'
import {StyleSheet, TouchableOpacity, Image, Text, View} from 'react-native'
import {
    RkText,
    RkButton,
    RkChoice,
    RkTheme,
    RkCard,
    RkSeparator
} from 'react-native-ui-kitten';
import QRCode from 'react-native-qrcode'

export default class ContractDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userType: 'buyer',
            contractState: this.props.navigation.state.params.contract.contractState
        }
    }

    componentWillMount() {
    }

    render() {
        const props = this.props.navigation.state.params;
        return (
            <View>
                <RkCard style={{margin : 10}}>
                    <View rkCardHeader>
                        <Text>{props.contract.address}</Text>
                    </View>
                    <View rkCardContent>
                        <RkText rkType='primary'> Buyer  -> {props.contract.buyer}</RkText>
                        <RkText rkType='primary'> Seller -> {props.contract.buyer}</RkText>
                        <RkText rkType='success'> Balance is {props.contract.balance.toString()}</RkText>
                        <RkText> Contract is <Text style={{color: '#'}}>{props.contract.contractState} </Text></RkText>
                        <RkText> Creation time {props.contract.date.toLocaleDateString()} </RkText>

                    </View>
                    <View style={{marginLeft : 20}}>
                        <QRCode
                            value={props.contract.address}
                            size={200}
                            bgColor='black'
                            fgColor='white'/>
                    </View>
                    <View rkCardFooter>
                        <View style={{marginBottom : 12}}>
                            <RkButton rkType="primary" style={{margin: 8}}> Confirm Purchase </RkButton>
                            <RkButton rkType="primary" style={{margin: 8}}> Item Received </RkButton>
                        </View>
                        <View>
                            <RkButton rkType="danger" style={{margin: 8}}> Abort</RkButton>
                            <RkButton rkType="danger" style={{margin: 8}}> Refund </RkButton>


                        </View>
                    </View>
                </RkCard>
            </View>
        )
    }
}
import React from 'react'
import { StyleSheet,TouchableOpacity,Image, Text, View } from 'react-native'
import {
    RkText,
    RkChoiceGroup,
    RkChoice,
    RkTheme,
    RkCard,

    RkSeparator
} from 'react-native-ui-kitten';
export default class Contract extends React.Component {

  constructor(props)
  {
    super(props)

      this.onTouchPress=this.onTouchPress.bind(this)

  }
 onTouchPress()
 {
   console.log(this)
     this.props.navigation.navigate('detail', { contract: this.props.contract })
 }

  render () {
    console.log("contract props", this.props)
    return (
      // <View source={{uri: styles.container}}>
      //   <RkText> Address {this.props.contract.address}</RkText>
      //   {this.props.contract.buyer ? <RkText> Buyer Address{this.props.contract.buyer}</RkText> : ''}
      //   <RkText> {this.props.contract.value} $oter</RkText>
      // </View>
        <TouchableOpacity onPress={this.onTouchPress}>
      <RkCard style={{ marginLeft:15, marginRight:15, marginBottom:8 }}>
      <View rkCardHeader>
        <Text>{this.props.contract.address}</Text>
      </View>
      <Image rkCardImg source={require('../../img/back.jpg')}/>
        <View rkCardContent>
            {this.props.contract.buyer ? <RkText> Buyer {this.props.contract.buyer}</RkText> : ''}
          <Text> Contract is {this.props.contract.contractState} </Text>
        </View>
          <View rkCardFooter>
    <Text>{this.props.contract.date.toDateString()}</Text>
      </View>
      </RkCard>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderColor: '#666',
    padding: 10,
    margin: 10,
    borderWidth: 1
  }
})

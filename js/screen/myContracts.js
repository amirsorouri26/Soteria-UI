/**
 * Created by mohsen on 8/2/2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux'
import  {Text, View, Button,StyleSheet,Image} from 'react-native'
import TimerMixin from 'react-timer-mixin';
import ContractList from './contract/ContractList'
export  class MyContracts extends Component {
    static navigationOptions = {
        title: 'Contracts'
        ,
        tabBarIcon: ({ tintColor }) => (
            <Image
                source={require('../img/contract.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    mixins: [TimerMixin]

    componentWillMount() {
        console.log('myContract',this)
        var navigator = this.props.navigation;
        console.log(this.props)

    }

    constructor(props) {
        super(props)
        this.btnclick = this.btnclick.bind(this)
        this.btnLogIn = this.btnLogIn.bind(this)
    }

    state = {
        publickey: this.props.wallet.publicCode
        ,
        soter: this.props.wallet.soters
    }

    btnclick() {
        this.setState({test: !this.state.test})
    }

    btnLogIn() {

        this.props.navigation.navigate('MainPage')
    }

    render() {
        let count = 0;
        console.log('walletstate', this.state)
        const contracts = [
            {
                id: 1, address: '0x12019201920', seller : '0x00019201', buyer: '0x12019201920',
                balance: 12100000, date: new Date(2016, 5, 12), contractState : 'created'
            },
            {
                id: 2, address: '0xe2019b01a20', seller : '0x00019201', buyer: '0x12019201920',
                balance: 12900000, date: new Date(2014, 5, 11),
                contractState : 'locked'
            },
            {
                id: 3, address: '0x1201920b19e0',seller : '0x00019201',  buyer: '0x12019201920',
                balance: 12500000, date: new Date(2017, 5, 16),
                contractState : 'released'

            }
        ]
        return (
            <View>

                <ContractList contracts={contracts} navigation={this.props.navigation} />

            </View>)

    }
}
const mapStateToPropsMainWallet = (state) => {
    return {
        wallet: state.soteria.myWallet
    }
}
const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25,

    },
});
export default connect(mapStateToPropsMainWallet)(MyContracts);